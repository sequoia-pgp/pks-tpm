#+TITLE: Private Key Store: TPM
#+PROPERTY: header-args :tangle yes :exports both
#+HTML_HEAD: <link rel="stylesheet" href="https://sandyuraz.com/styles/org.min.css">

This crate exposes Private Key Store endpoints for TPM keys.

Each TPM provided key should be a =yml= file in the current
directory. See https://wiktor.gitlab.io/tpm-openpgp/ for the
instructions on how to express and manage TPM keys.

Running is as simple as:

#+BEGIN_SRC sh
set -e
set -o pipefail

tpm_server &
cargo run &
sleep 5

tpm2_startup -c -T mssim

TCTI=mssim:
PATH=$PATH:./target/debug

# Increase verbosity of commands
export RUST_LOG=info
#+END_SRC

Then, each key will be exposed via HTTP:

#+BEGIN_SRC sh
curl http://localhost:3000/sample-key
#+END_SRC

This crate additionally exposes a Private Key Store
(https://gitlab.com/wiktor/pks) endpoint that can be used with the
=sq= command line tool (see =sq sign --private-key-store= and =sq
decrypt --private-key-store=).

For more advanced usage like creating OpenPGP keys and artifacts out
of TPM managed keys (or any other key available via PKS) see
https://gitlab.com/wiktor/sq-pks

* API

** Key management
  
Retrieving keys:

#+BEGIN_SRC sh :results output raw
curl http://localhost:3000/sample-key
#+END_SRC

#+RESULTS:
#+begin_example
---
spec:
  provider:
    tpm:
      tcti: "device:/dev/tpmrm0"
      handle: ~
      parent: ~
      private: ~
      unique:
        RSA:
          bytes: cd1abae5d734341ad373bae4f9ef46b1cf699d4054c859b9c0f0c811ca4d7b1cb03c66ea655156639b78c5db2c2fea42430f417ab3d4aee5f63b881dd106a3c60105bc46bb18c7a794a17f50392405551f77287e61b5f784354cd351021e1853b0cfd3470d4cc9bd9e39836b83c1be6bb200fef56786406e8cd45f73e4a9f523
      wrapped: ~
      policy: ""
  algo:
    RSA:
      bits: 1024
      exponent: ~
  private:
    rsa:
      prime: f69495352f2ab58db89a0a6ddb060ca0baa5ec190d1d61f0fae32cdfb7516fc9e4968b5c494c057f35dfe69136fe35434f0a3b8979551347c47a357abad0ad0b
      modulus:
        bytes: cd1abae5d734341ad373bae4f9ef46b1cf699d4054c859b9c0f0c811ca4d7b1cb03c66ea655156639b78c5db2c2fea42430f417ab3d4aee5f63b881dd106a3c60105bc46bb18c7a794a17f50392405551f77287e61b5f784354cd351021e1853b0cfd3470d4cc9bd9e39836b83c1be6bb200fef56786406e8cd45f73e4a9f523
  capabilities:
    - sign
  auth: "123"
#+end_example

Updating keys:

#+BEGIN_SRC sh
curl --data-binary @sample-key.yml http://localhost:3000/sample-key-2
#+END_SRC

** Cryptographic operations

Retrieving signing capability:

#+BEGIN_SRC sh :results output raw
curl -i --data-binary 1234 "http://localhost:3000/sample-key?capability=sign"
#+END_SRC

#+RESULTS:
#+begin_example
HTTP/1.1 200 OK
location: http://localhost:3000/sample-key/sign
content-length: 0
date: Mon, 15 Nov 2021 13:54:48 GMT

#+end_example

Retrieving decrypt capability:

#+BEGIN_SRC sh :results output raw
curl -i --data-binary 1234 "http://localhost:3000/sample-key?capability=decrypt"
#+END_SRC

#+RESULTS:
#+begin_example
HTTP/1.1 200 OK
location: http://localhost:3000/sample-key/decrypt
content-length: 0
date: Mon, 15 Nov 2021 13:54:55 GMT

#+end_example

** Key migration

Key migration can be achieved using the =wrap= API. Note that the
wrapping key need to be a restricted key and needs to be available for
the wrap to succeed:

#+begin_src yaml :tangle new-parent.yml
spec:
  provider:
    tpm:
      tcti: "mssim:"
      handle: ~
      parent: ~
      private: ~
      unique:
        RSA:
          bytes: bd1fdfb6ad445dd24bd9150886a7ea392863bf2864f8105bca870349150691309581f08271d93a7286e6d8126df38ca51b4d5366a867461743c842d0e4d6867cc81e8d8a96b6c7b01d702d1674d6432ac686d9e1d9b767b46d93d640c9ddcf952c46690231711ccd040d7b85453acb9a857040f49208823315b970e0ec3c15f31a1d6d17238a6b1e717020946ba2e8591f5aa36a3d65b4ac166755e54609355c2517dafc6c545f322093dd6ad01b33931c9f25ef3e47e61bf5d2a2b553af3fef8c2180267b76857768d38e5954b90362923df57ded9a9264cc56a120c48d2f47e6d7dc7a069f2a2c7b4d4079a599df8e672bca9540dcd024bcdd45cfc6450653
  algo:
    RSA:
      bits: 2048
      exponent: ~
  private: ~
  capabilities:
    - decrypt
    - restrict
  auth: "123"
#+end_src
  
#+BEGIN_SRC sh :results output raw
curl -X PUT --data-binary @new-parent.yml "http://localhost:3000/new-parent"
#+END_SRC

Then, we can take any key and wrap it using the =wrap= endpoint:

#+BEGIN_SRC sh
curl --data-binary @sample-key.yml "http://localhost:3000/new-parent/wrap"
#+END_SRC

#+RESULTS:
#+begin_example
---
spec:
  provider:
    tpm:
      tcti: "mssim:"
      handle: ~
      parent: ~
      private: ~
      unique:
        RSA:
          bytes: cd1abae5d734341ad373bae4f9ef46b1cf699d4054c859b9c0f0c811ca4d7b1cb03c66ea655156639b78c5db2c2fea42430f417ab3d4aee5f63b881dd106a3c60105bc46bb18c7a794a17f50392405551f77287e61b5f784354cd351021e1853b0cfd3470d4cc9bd9e39836b83c1be6bb200fef56786406e8cd45f73e4a9f523
      wrapped:
        secret: 50d19bf1d20cdd806625c104f2e84991a1d211f5d19a9d3e1b8c4e5e465eb21ed109a2bba7a9b2f3ffd9cf6d5f6d645c26775a945077632fa44833db59f374694cc2c6375331fe28bc5f9f4878076b4d4bb2d3999aed90bd02884372f106e239bb3c08a89dd47838ce5eb58b637b9c920a31bc49c2aae8033ba39856668da7d6dfe78861ff1ffa53a6ae0062c655b39cc5f11fc4624789b8726cf67628e5044758886a936e6908536969db8fb333becdea0bc5568fa1cba0d53a09fb95968cce3ce78a4237a5164c2f2278cebe848e6f6a14a1964e9d0ff0a3c55577a74c26608bf63986a8ce2b67a705aa41ee262362c6da17731b38bc774c2af3a2f6050d3d
        private: 0020731b28749cca8180938fe85673b0436fffcf45709d076b9edf4cc23dff2437bc85faad28d11abe9d611df292b91f9f1fe3dc56b5945d8fdb5ecc75b8f5a93a0b77ac8b38d28b5a02e087de10ffc90de3291f9e62c79ae5d978d419ea7c341fecc2c1a8efd48290d76d601f127972b97f84184b976afec22aaca711f14c6ca44e6c24b1de01d3fe7b81c9
        data: ""
      policy: 09bd2ec618ec5d4688b2861cd8aedbbce1c1dd0b9e31e4a12f837750b33831e2
  algo:
    RSA:
      bits: 1024
      exponent: ~
  private: ~
  capabilities:
    - sign
  auth: "123"
#+end_example
