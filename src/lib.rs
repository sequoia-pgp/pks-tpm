#![deny(missing_docs)]
//! Private Key Store TPM provider.
//!
//! Defines HTTP handler that implements the [Private Key Store protocol] using the
//! TPM crate.
//!
//! [Private Key Store protocol]: https://gitlab.com/sequoia-pgp/pks

use hyper::{Body, Request, Response};
use std::path::Path;
use tpm_openpgp::AlgorithmSpec;

/// PKS key handler.
///
/// Handles requests to keys using the TPM provider.
pub struct Handler<'a> {
    keys_dir: &'a Path,
}

impl<'a> Handler<'a> {
    /// Construct a new `Handler` with keys stored in `keys_dir`.
    pub fn new(keys_dir: &'a Path) -> Self {
        Self { keys_dir }
    }

    /// Handles key requests using PKS.
    pub async fn handle(&self, req: Request<Body>) -> hyper::Result<Response<Body>> {
        let uri = &req.uri().to_string()[1..];
        let parts = uri.split('?').collect::<Vec<_>>()[0];
        let parts = parts.split('/').collect::<Vec<_>>();
        let child = self.keys_dir.with_file_name(parts[0]).with_extension("yml");

        if parts.len() == 1 && req.method() == hyper::Method::PUT {
            use std::io::Write;
            let body = hyper::body::to_bytes(req.into_body()).await?;
            let deserialized: tpm_openpgp::Description = serde_yaml::from_slice(&body).unwrap();

            let mut output = std::fs::File::create(&child).unwrap();
            output.write_all(&body).unwrap();

            let contents = serde_yaml::to_string(&deserialized).unwrap();

            return Ok(Response::new(Body::from(contents)));
        }

        if !child.is_file() || !child.exists() {
            return Ok(Response::builder()
                .status(http::StatusCode::NOT_FOUND)
                .body(Default::default())
                .unwrap());
        }

        let mut deserialized: tpm_openpgp::Description =
            serde_yaml::from_reader(std::fs::File::open(&child).unwrap()).unwrap();

        if parts.len() == 1 && req.method() == hyper::Method::GET {
            tpm_openpgp::read_key(&mut deserialized.spec).unwrap();

            let contents = serde_yaml::to_string(&deserialized).unwrap();

            Ok(Response::new(Body::from(contents)))
        } else if parts.len() == 2 && parts[1] == "public" {
            tpm_openpgp::read_key(&mut deserialized.spec).unwrap();

            let (body, content_type) = match deserialized.spec.provider.tpm.unique {
                Some(tpm_openpgp::PublicKeyBytes::RSA(bytes)) => (
                    hex::decode(bytes.bytes).unwrap(),
                    "application/vnd.pks.public.rsa.modulus",
                ),
                Some(tpm_openpgp::PublicKeyBytes::EC(ec)) => {
                    let mut public = vec![0x04];
                    public.extend(hex::decode(ec.x).unwrap());
                    public.extend(hex::decode(ec.y).unwrap());
                    (public, "application/vnd.pks.public.p256.compressed")
                }
                _ => {
                    return Ok(Response::builder()
                        .status(http::StatusCode::NOT_FOUND)
                        .body(Default::default())
                        .unwrap())
                }
            };
            Ok(Response::builder()
                .header("Content-Type", content_type)
                .body(Body::from(body))
                .unwrap())
        } else if parts.len() == 1 && req.method() == hyper::Method::POST {
            let mut resp = Response::default();
            let usage = if req
                .uri()
                .query()
                .unwrap_or_default()
                .contains("capability=sign")
            {
                "sign"
            } else if req
                .uri()
                .query()
                .unwrap_or_default()
                .contains("capability=decrypt")
            {
                "decrypt"
            } else {
                panic!("Unknown capability requested.");
            };
            let host =
                String::from_utf8_lossy(req.headers().get("host").unwrap().as_bytes()).to_string();
            resp.headers_mut().insert(
                "Location",
                hyper::header::HeaderValue::from_str(
                    &http::Uri::builder()
                        .scheme("http")
                        .authority(host)
                        .path_and_query(format!("/{}/{}", parts[0], usage))
                        .build()
                        .unwrap()
                        .to_string(),
                )
                .unwrap(),
            );
            resp.headers_mut().insert(
                "Accept-Post",
                hyper::header::HeaderValue::from_str("application/vnd.pks.digest.sha256").unwrap(),
            );

            Ok(resp)
        } else if parts.len() == 2 && req.method() == hyper::Method::POST && parts[1] == "sign" {
            let body = hyper::body::to_bytes(req.into_body()).await?;
            eprintln!("Signing bytes: {:?} ({})", body, body.len());
            let sig = tpm_openpgp::sign(&deserialized.spec, &body).unwrap();
            eprintln!("Signature: {:?} ({})", sig, sig.len());
            Ok(Response::new(Body::from(sig)))
        } else if parts.len() == 2 && req.method() == hyper::Method::POST && parts[1] == "decrypt" {
            let body = hyper::body::to_bytes(req.into_body()).await?;
            eprintln!("Ciphertext bytes: {:?} ({})", body, body.len());
            let plaintext = match deserialized.spec.algo {
                AlgorithmSpec::Rsa { .. } => {
                    tpm_openpgp::decrypt(&deserialized.spec, &body).unwrap()
                }
                AlgorithmSpec::Ec { .. } => {
                    let body = if body[0] == 0x04 {
                        // compressed point
                        body.slice(1..)
                    } else {
                        body
                    };
                    eprintln!("Derive bytes: {:?} ({})", body, body.len());
                    tpm_openpgp::derive(&deserialized.spec, &body).unwrap().0
                }
            };
            eprintln!("Plaintext bytes: {:?} ({})", plaintext, plaintext.len());
            Ok(Response::new(Body::from(plaintext)))
        } else if parts.len() == 2 && req.method() == hyper::Method::POST && parts[1] == "wrap" {
            let body = hyper::body::to_bytes(req.into_body()).await?;
            let parent = deserialized;
            let mut deserialized: tpm_openpgp::Description = serde_yaml::from_slice(&body).unwrap();
            tpm_openpgp::wrap(&mut deserialized.spec, &parent.spec).unwrap();
            let contents = serde_yaml::to_string(&deserialized).unwrap();
            Ok(Response::new(Body::from(contents)))
        } else {
            Ok(Response::builder()
                .status(http::StatusCode::NOT_FOUND)
                .body(Default::default())
                .unwrap())
        }
    }
}
