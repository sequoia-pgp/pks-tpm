use hyper::{
    service::{make_service_fn, service_fn},
    Server,
};
use std::error::Error;
use std::net::SocketAddr;
use std::sync::Arc;

use pks_tpm::Handler;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let handler = Arc::new(Handler::new(std::path::Path::new(".")));

    let make_svc = make_service_fn(|_| {
        let handler = Arc::clone(&handler);
        async move {
            Ok::<_, std::convert::Infallible>(service_fn(move |req| {
                let handler = Arc::clone(&handler);
                async move { handler.handle(req).await }
            }))
        }
    });

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    Server::bind(&addr).serve(make_svc).await?;

    Ok(())
}
